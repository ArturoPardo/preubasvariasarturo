package es.vqs.first;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class PruebasXML {

	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setIgnoringComments(true);
		factory.setIgnoringElementContentWhitespace(true);
		DocumentBuilder builder = factory.newDocumentBuilder();
		File fichero = new File(
				"C:\\Users\\VQuiles\\Downloads\\Materiales Ejercicios XML\\Materiales Ejercicios\\Ejercicio XML.xml");
		Document doc = builder.parse(fichero);
		Node raiz = doc.getFirstChild();
		PruebasXML.leerNodo(raiz);
	}

	public static void leerNodo(Node nodo) {
		Node hijo;
		NodeList raiz = nodo.getChildNodes();
		System.out.println(nodo.getNodeName());
		PruebasXML.leerAtributos(nodo.getAttributes());
		for (int i = 0; i < raiz.getLength(); i++) {
			hijo = raiz.item(i);
			if (hijo.getNodeType() == 3) {
				System.out.println(hijo.getNodeValue());
			} else {
				PruebasXML.leerNodo(hijo);
			}
		}
	}

	private static void leerAtributos(NamedNodeMap atributos) {
		for (int i = 0; atributos != null && i < atributos.getLength(); i++) {
			Node hijo = atributos.item(i);
			if (hijo.getNodeType() == 3) {
				System.out.println(hijo.getNodeValue());
			} else {
				PruebasXML.leerNodo(hijo);
			}
		}
	}

}
