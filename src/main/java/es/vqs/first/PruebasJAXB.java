package es.vqs.first;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import es.vqs.first.entity.jaxb.Html;

public class PruebasJAXB {
	public static void main(String args[]) throws JAXBException {
		File fichero = new File(
				"C:\\Users\\VQuiles\\Downloads\\Materiales Ejercicios XML\\Materiales Ejercicios\\Ejercicio XML.xml");
		JAXBContext context = JAXBContext.newInstance(Html.class);
		Unmarshaller unmarshaller = context.createUnmarshaller();
		Html raiz = (Html) unmarshaller.unmarshal(fichero);
		
		System.out.println(raiz);
	}
}
