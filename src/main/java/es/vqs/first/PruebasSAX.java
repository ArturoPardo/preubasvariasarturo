package es.vqs.first;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;

public class PruebasSAX {

	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser parser = factory.newSAXParser();
		ManejadorSAX manejador = new ManejadorSAX();
		File fichero = new File(
				"C:\\Users\\VQuiles\\Downloads\\Materiales Ejercicios XML\\Materiales Ejercicios\\Ejercicio XML.xml");
		parser.parse(fichero, manejador);
	}



}
