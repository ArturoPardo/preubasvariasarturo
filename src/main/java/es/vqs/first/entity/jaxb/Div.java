package es.vqs.first.entity.jaxb;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

public class Div implements Serializable {
	
	private static final long serialVersionUID = -8220841005326868008L;
	private String[] value;
	private String style;

	@XmlValue
	public String[] getValue() {
		return this.value;
	}

	public void setValue(String[] _value) {
		this.value = _value;
	}

	@XmlAttribute
	public String getStyle() {
		return this.style;
	}

	public void setStyle(String _style) {
		this.style = _style;
	}

}
