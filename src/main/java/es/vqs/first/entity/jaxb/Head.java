package es.vqs.first.entity.jaxb;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

@XmlRootElement
public class Head implements Serializable {

	private static final long serialVersionUID = 1L;
	private String[] value;

	@XmlValue
	public String[] getValue() {
		return this.value;
	}

	public void setValue(String[] _value) {
		this.value = _value;
	}
}
