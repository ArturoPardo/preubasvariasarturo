package es.vqs.first.entity;

public class Body extends CommonNode {
	private Div[] divs;
	private int contador;

	public Body(int numHijos) {
		this.divs = new Div[numHijos];
		this.contador = 0;
	}

	public Div[] getDivs() {
		return divs;
	}

	public void setDivs(Div[] divs) {
		this.divs = divs;
	}
	
	public void addDiv(Div nuevoNodo) {
		if (this.contador < this.divs.length) {
			this.divs[this.contador] = nuevoNodo;
			this.contador++;
		}
	}
}
